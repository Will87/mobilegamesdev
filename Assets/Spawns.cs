﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawns : MonoBehaviour {
    private float delay;
    private float repeat;
    public GameObject sphere;
   
	void Start () {
        delay = 0;
        repeat =4;
        InvokeRepeating("Spawn",delay,repeat);     
    }
	
	void Spawn () {
        Instantiate(sphere, new Vector3(-30, Random.Range(-20, 20), 25), Quaternion.identity);
        
        //for test and spawning the balls in the center
      //  Instantiate(sphere, new Vector3(-30, -5, 25), Quaternion.identity);
    }

    public void setDelay(float newDelayValue)
    {
        this.delay = newDelayValue;
        print("New delay Value: " + newDelayValue.ToString());
    }

    public void setRepeat(float newRepeatValue)
    {
        this.repeat = newRepeatValue;
        print("New repeat Value: " + newRepeatValue.ToString());
    }

}
