﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliEnemySide : MonoBehaviour {

    void OnCollisionEnter(Collision collisionInfo)
    {
        print("Detected collision between " + gameObject.name + " and " + collisionInfo.collider.name);
        //    print("There are " + collisionInfo.contacts.Length + " point(s) of contacts");
        //    print("Their relative velocity is " + collisionInfo.relativeVelocity);
        if (collisionInfo.gameObject.name == "sidePlane")
        {
            Destroy(this.gameObject);
            print("SidePlane Hit");
        }
    }
}
