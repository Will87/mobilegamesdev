﻿using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour {
    GameObject targetItem;
    ControlObject selectedObject;
    ControlObject draggedObject;
    ControlObject zoomedObject;
    ControlObject rotatedObject;

    private float rotX= 0f;
    private float rotY= 0f;
    private Vector3 origRot;
    

    public float rotSpeed = 0.5f;
    public float dir = 1f;


    Plane dragPlane;
    int timer = 0;
    private bool going = true;
    private bool firstDoubleTouch;
    private float firstDistance;

    Vector2 firstCamDistance;
    Vector2 secCamDistance;

    public Camera cam; // The canvas
    public float zoomSpeed = 0.5f;        // The rate of change of the canvas scale factor


    void Start () {
        origRot = cam.transform.eulerAngles;
        rotX = origRot.x;
        rotY = origRot.y;
    }

	void Update () {
        int latestTouch = 0;
        if (Input.touchCount > 0 && GameObject.Find("Sphere").transform.position.z < 5)
        {
            
            Touch first = Input.touches[0];
            Touch currentTouch = Input.GetTouch(latestTouch);
            RaycastHit hit;

            Ray ourRay = Camera.main.ScreenPointToRay(first.position);
            Ray ourRayd = Camera.main.ScreenPointToRay(first.position);

            // Debug.DrawRay(ourRay.origin, ourRay.direction,Color.red);

            if (first.phase == TouchPhase.Began)
            {
                if (Physics.Raycast(ourRay, out hit))
                {
                    if (selectedObject) selectedObject.Unselect();
                    selectedObject = hit.collider.gameObject.GetComponent<ControlObject>();
                    selectedObject.Selected();
                    print("Hit");

                    draggedObject = hit.collider.gameObject.GetComponent<ControlObject>();
                    draggedObject.SelectedForDrag();
                    print("Hit For Drag");
                    //look into this, making shapes teleport to next touch
                    dragPlane = new Plane(-transform.forward, draggedObject.transform.position);
                    going = true;
                }
                else if (selectedObject)
                {
                    selectedObject.Unselect();
                    draggedObject.Unselect();
                    selectedObject = null;
                    draggedObject = null;
                }
            }//end touchphase began

            if (Input.touchCount == 2)
            {
                // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);
                if (firstDoubleTouch)
                {
                    firstDistance = Vector3.Distance(touchZero.position, touchOne.position);
                    firstCamDistance = touchZero.position - touchOne.position;
                    secCamDistance = touchZero.position - touchOne.position;
                    firstDoubleTouch = false;
                }
                float currentDistance = Vector3.Distance(touchZero.position, touchOne.position);


                // Vector2 touch0_prevPos = touch0.position - touch0.deltaPosition;
                //Vector2 touch1_prevPos = touch1.position - touch1.deltaPostion;

                float deltaMagnitudeDiff = (currentDistance / firstDistance);

                if (selectedObject)
                {
                    selectedObject.scaleFromTouch(deltaMagnitudeDiff);
                }
            }//end touchcount2

            //if (Input.touchCount == 1)
            else
            {
                if (first.phase == TouchPhase.Moved)
                {
                    if (draggedObject)
                    {
                        draggedObject.Drag(first.deltaPosition, transform);

                        float distance;

                        Debug.DrawRay(ourRayd.origin, 100 * ourRay.direction, Color.red);

                        dragPlane.Raycast(ourRayd, out distance);

                        draggedObject.transform.position = ourRayd.GetPoint(distance);
                    }
                       else
                         {
                        //CAMERA
                        //targetItem.transform.Rotate(0, first.deltaPosition.x * 1, 0, Space.World);
                        float deltaX = first.position.x - currentTouch.position.x;
                        float deltaY = first.position.y - currentTouch.position.y;
                        rotX -= deltaX * Time.deltaTime * rotSpeed * dir;
                        rotY += deltaY * Time.deltaTime * rotSpeed * dir;
                        cam.transform.eulerAngles = new Vector3(rotX, rotY, 0f);
                         }
                     
                    firstDoubleTouch = true;
                }
            }//end else
            latestTouch++;
        }//end touch count > 0 
        //else no touches
        else
        {
            int distOut = 34;
            if (GameObject.Find("Sphere").transform.position.z >= distOut)
            {
                print("Gone through");
                selectedObject.scaleFromTouch(.75f);
                selectedObject.Unselect();
                timer++;
                print("Timer: " + timer.ToString());
                if (timer > 75)
                {
                    going = false;
                    print("going false");
                    GameObject.Find("Sphere").transform.position = new Vector3(18, -5, 0);
                    timer = 0;
                }
            }
            else if ((GameObject.Find("Sphere").transform.position.z <= distOut) && (going == true))
            {
                print("going true");
                var spherePos = selectedObject.transform.position;
                selectedObject.transform.position = new Vector3(spherePos.x, spherePos.y, spherePos.z + 1);
                // firstDoubleTouch = true;
            }
        }//end else
     
    }//end update
}//end class
