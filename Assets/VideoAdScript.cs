﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

//[RequireComponent(typeof(Button))]
public class VideoAdScript : MonoBehaviour

{

    

    public void showAd()
    {
        Advertisement.Show("", new ShowOptions() { resultCallback = HandleShowResult });
    }

    public void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                //Debug.Log("Video completed. Offer a reward to the player.");
                TimeScript.instance.UpdateTime(1000);

                break;
            case ShowResult.Skipped:
              //  Debug.LogWarning("Video was skipped.");
                break;
            case ShowResult.Failed:
             //   Debug.LogError("Video failed to show.");
                break;
        }
    }
}
