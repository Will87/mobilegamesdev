// <copyright file="GPGSIds.cs" company="Google Inc.">
// Copyright (C) 2015 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//    limitations under the License.
// </copyright>

///
/// This file is automatically generated DO NOT EDIT!
///
/// These are the constants defined in the Play Games Console for Game Services
/// Resources.
///


public static class ZorbAchieveResources
{
        public const string achievement_test5 = "CgkIibj-4L4YEAIQBQ"; // <GPGSID>
        public const string achievement_test3 = "CgkIibj-4L4YEAIQAw"; // <GPGSID>
        public const string leaderboard_high_score = "CgkIibj-4L4YEAIQBw"; // <GPGSID>
        public const string achievement_test_1 = "CgkIibj-4L4YEAIQAQ"; // <GPGSID>
        public const string achievement_test_2 = "CgkIibj-4L4YEAIQAg"; // <GPGSID>
        public const string achievement_test4 = "CgkIibj-4L4YEAIQBA"; // <GPGSID>

}

