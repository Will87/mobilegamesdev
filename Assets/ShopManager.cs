﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;

public class ShopManager : MonoBehaviour {

   // bool IsConnectedToGoogleServices;

    private void Start()
    {
       // IsConnectedToGoogleServices = false;
      //  PlayGamesPlatform.Activate();
       // PlayGamesPlatform.DebugLogEnabled = true;
       // ConnectToGoogleServices();
        //   SceneManager.Instance.ConnectToGoogleServices();
    }

   /*public bool ConnectToGoogleServices()
    {
        if (!IsConnectedToGoogleServices)
        {
            Social.localUser.Authenticate((bool success) => {
                IsConnectedToGoogleServices = success;
            });
        }
        return IsConnectedToGoogleServices;
    }*/

    public void buyTime()
    {
        AchievementManager.Instance.Achieve3();
        IAPManager.Instance.BuyExtraTime();
    }

    public void buyNoAds()
    {
        IAPManager.Instance.BuyNoAds();
    }

    public void returnToMenu()
    {
        Application.LoadLevel("scene1");
    }
}
