﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeScript : MonoBehaviour {

    public static TimeScript instance;
    public Text timeText;
    public float timeLeft = 1000;

    // Use this for initialization
    void Start () {
        UpdateTime();
        //x = Time.unscaledTime;
	}

    private void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update () {
        if (timeLeft > 0)
        {
            print("TIME WORKING");
            timeLeft -= 1;
            UpdateTime();           
        }
        else if (timeLeft <= 0)
        {
            print("TIMES UP");
            //instance.loadGameOver();
         Application.LoadLevel("scene2");
        }
    }

    public void UpdateTime()
    {
        timeText.text = "Time Remaining: " + timeLeft.ToString();
    }

    public void UpdateTime(float extraTime)
    {
        timeLeft = timeLeft + extraTime;
        timeText.text = "Time Remaining: " + timeLeft.ToString();
    }
}
