﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ScoreScript : MonoBehaviour {

    public static ScoreScript instance;
    public Text scoreText;
    int score;

 
    void Start () {
        score = 0;
        UpdateScore();
	}

    private void Awake()
    {
        instance = this;
    }

    public void AddScore(int newScoreValue){

        score += newScoreValue;
        UpdateScore();
        }

	void UpdateScore () {
        scoreText.text = "Score: " + score.ToString();
	}
}
