﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;

public class PlayServicesScript : MonoBehaviour {

    static PlayServicesScript Instance;
    bool IsConnectedToGoogleServices = false;

    public void Start()
    {
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.DebugLogEnabled = true;
        ConnectToGoogleServices();
    }

    public void ConnectToGoogleServices()
    {
        // if (!IsConnectedToGoogleServices)
        // {
        Social.localUser.Authenticate((bool success) => {
            IsConnectedToGoogleServices = success;
        });

        //  }
        //return IsConnectedToGoogleServices;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
