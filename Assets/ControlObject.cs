﻿using UnityEngine;
using System.Collections;
using System;

public class ControlObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void Selected()
    {
        GetComponent<Renderer>().material.color = Color.red;
    }

    internal void Unselect()
    {
        GetComponent<Renderer>().material.color = Color.white;
    }

    internal void SelectedForDrag()
    {
        print("Selected ");
    }

    internal void Drag(Vector2 deltaPosition, Transform cameraTransform)
    {
        transform.position += Vector3.Distance(transform.position, cameraTransform.position)* (deltaPosition.x * cameraTransform.right + deltaPosition.y * cameraTransform.up) * 0.0025f; 

    }

    internal void scaleFromTouch(float deltaMagnitudeDiff)

    {
          float min = 3;
          float max = 10;
          float scalespeed = deltaMagnitudeDiff * 5;
          transform.localScale = new Vector3(
                  Mathf.Clamp(scalespeed, min, max),
                  Mathf.Clamp(scalespeed, min, max),
                  Mathf.Clamp(scalespeed, min, max) );
    }


}
