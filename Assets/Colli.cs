﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Colli : MonoBehaviour {

    //  public ScoreScript scoreScript;
    // public int scoreValue = 5;
    // enemyScript enemyScriptInsta;
    public static Colli instance;
    public Text countText;
    public Text levelText;
    GameObject enemyBall;
    enemyScript enemyScriptInsta;
   public int count;
   public int level;

    void Start()
    {
         

        count = 0;
        level = 0;
       // countText = GameObject.FindGameObjectWithTag("CountText").GetComponent<Text>();
        setCountText();
        setLevelText();
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
          print("Detected collision between " + gameObject.name + " and " + collisionInfo.collider.name);
        //    print("There are " + collisionInfo.contacts.Length + " point(s) of contacts");
        //    print("Their relative velocity is " + collisionInfo.relativeVelocity);

        //if (collisionInfo.gameObject.name == "Sphere")
        if (collisionInfo.gameObject.name == "EnemySphere(Clone)")
        {



            Destroy(collisionInfo.gameObject);
           // Destroy(this.gameObject);
            print("BOOM");
            count = count + 1;
            setCountText();
            AddScoreToLeaderboard("CgkIibj - 4L4YEAIQBw",100);

            
            print("MADE IT");
            if (count% 2 == 0)
            {
                print("Modulus Count: " + count.ToString());
                level++;             
                setLevelText();
                print("Level Count: " + level.ToString());
                
            }
            if (level >= 2)
            {
             //   enemyScriptInsta.setMovement(1.0f);
                print("Speeding up ");
            }
            // scoreScript.AddScore(scoreValue);
            if (count == 3)
            {
                scoreAchievement();
            }
        }
    }

   /* void OnCollisionStay(Collision collisionInfo)
    {
      //  print(gameObject.name + " and " + collisionInfo.collider.name + " are still colliding");
    }

    void OnCollisionExit(Collision collisionInfo)
    {
     //   print(gameObject.name + " and " + collisionInfo.collider.name + " are no longer colliding");
    }*/

    void setCount()
    {
        this.count++;
    }

    void setCountText()
    {
        countText.text = "Count: " + count.ToString();

    }

    void setLevelText()
    {
        levelText.text = "Level: " + level.ToString();
    }

    public void scoreAchievement()
    {
        Social.ReportProgress(ZorbAchieveResources.achievement_test_1, 33.3f, (bool success) =>
          { });
    }

    public void AddScoreToLeaderboard(string leaderboardid, double score)
    {
        Social.ReportProgress(leaderboardid, score, success => { });
    }

    void Update()
    {

        enemyBall = GameObject.Find("EnemySphere(Clone)");
    
      enemyScriptInsta = enemyBall.GetComponent<enemyScript>();

        

        print("TEST XXX ");
        print("LEVEL: " + level.ToString());
        if (level >= 2)
        {
               enemyScriptInsta.setMovement(1.0f);
            enemyScriptInsta.moveSpeed = .3f;
            print("Speeding up from update loop");
        }
        if (level >= 3)
        {
            enemyScriptInsta.setMovement(1.0f);
            enemyScriptInsta.moveSpeed = .5f;
            print("Speeding up from update loop");
        }
        if (level >= 4)
        {
            enemyScriptInsta.setMovement(1.0f);
            enemyScriptInsta.moveSpeed = .7f;
            print("Speeding up from update loop");
        }
        if (level >= 5)
        {
            enemyScriptInsta.setMovement(1.0f);
            enemyScriptInsta.moveSpeed = .9f;
            print("Speeding up from update loop");
        }
        if (level >= 6)
        {
            enemyScriptInsta.setMovement(1.0f);
            enemyScriptInsta.moveSpeed = 1.1f;
            print("Speeding up from update loop");
        }
        if (level >= 6)
        {
            enemyScriptInsta.setMovement(2.0f);
            enemyScriptInsta.moveSpeed = 2.0f;
            print("Speeding up from update loop");
        }
        print("TEST YYY ");
        print("LEVEL: " + level.ToString());
    }
}
