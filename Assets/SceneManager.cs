﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
public class SceneManager : MonoBehaviour {

    public static SceneManager Instance;
    bool IsConnectedToGoogleServices;

    public void Start()
    {
        IsConnectedToGoogleServices = false;
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.DebugLogEnabled = true;
        ConnectToGoogleServices();
    }

    public bool ConnectToGoogleServices()
    {
        if (!IsConnectedToGoogleServices)
        {
            Social.localUser.Authenticate ((bool success) => {
                IsConnectedToGoogleServices = success;
          });

       }
        return IsConnectedToGoogleServices;
    }

	// Use this for initialization
	/*void Start () {
		if(Instance != null)
        {
            GameObject.Destroy(gameObject);
        }
        else
        {
            GameObject.DontDestroyOnLoad(gameObject);
            Instance = this;
        }
	}*/

    public void loadMainMenu()
    {
        //Debug.Log("You pressed loadmenu");
        Application.LoadLevel("scene1");
    }

    public void loadGameOver()
    {
     //   Debug.Log("Game Over loaded");
        Application.LoadLevel("scene2");

    }
    public void loadGameScreen()
    {
     //   Debug.Log("You pressed Game begin");
        Application.LoadLevel("scene3");

    }

    public void loadShopScreen()
    {
        Debug.Log("You pressed shop button");
        Application.LoadLevel("scene4");
    }

    public void ToAchievement()
    {
        Debug.Log("You pressed Achievements");
        if (Social.localUser.authenticated)
        {
            Debug.Log("User Authenticated, trying to show Achievements");
            Social.ShowAchievementsUI();
        }
        else
        {
            Debug.Log("Failed to show Achievements");
        }

    }

    // Update is called once per frame
    void Update () {

        ConnectToGoogleServices();


        if (Input.GetKey(KeyCode.A))
        {

            Application.LoadLevel("scene1");
        }
        if (Input.GetKey(KeyCode.W))
        {
            Application.LoadLevel("scene3");

        }

        if (Input.GetKey(KeyCode.D))
        {
            Application.LoadLevel("scene2");

        }
    }
}
