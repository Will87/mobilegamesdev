﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementManager : MonoBehaviour {

    public static AchievementManager Instance;
	// Use this for initialization
	void Start () {
		
	}


    public void Achieve1()
    {
        Social.ReportProgress(ZorbAchieveResources.achievement_test_1, 100.0f, (bool success) =>
        { });
    }
    public void Achieve2()
    {
        Social.ReportProgress(ZorbAchieveResources.achievement_test_2, 100.0f, (bool success) =>
        { });
    }
    public void Achieve3()
    {
        Social.ReportProgress(ZorbAchieveResources.achievement_test3, 100.0f, (bool success) =>
        { });
    }


    // Update is called once per frame
    void Update () {
		
	}
}
