﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyScript : MonoBehaviour {

    public float moveSpeed;
   
    void Start () {
       moveSpeed = 0.1f;
    }

    public void setMovement(float newMoveValue)
    {
        moveSpeed = newMoveValue;
        print("New Move Speed: " + newMoveValue.ToString());
    }

    void Update () {
      
        var spherePos = this.transform.position;
        
        transform.position = new Vector3(spherePos.x + moveSpeed, spherePos.y, spherePos.z);

        /*transform.position = new Vector3(
                  (spherePos.x + moveSpeed),
                  Mathf.Clamp(spherePos.y, -20, 20),
                  Mathf.Clamp(spherePos.z, 20, 30));*/
    }

    
}
