﻿using UnityEngine;
using System.Collections;

public class Keybinds : MonoBehaviour
{
    public Vector2 startPos;
    public Vector2 direction;
    public bool directionChosen;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        int moveSpeed = 1;
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * moveSpeed;

        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.localScale += Vector3.up * moveSpeed;

        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * moveSpeed;

        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.localScale += Vector3.down * moveSpeed;

        }


        if (Input.GetKey(KeyCode.KeypadEnter))
        {
            transform.Rotate(Vector3.up, moveSpeed * 1);

        }
        if (Input.GetKey(KeyCode.Space))
        {
            jump();
        }
    }

         void jump()
    {
        int jumpSpeed = 1;   
            transform.position += Vector3.up * jumpSpeed;
            transform.position += Vector3.down * jumpSpeed;
    }
}